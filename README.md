# K40-RPI-CNC

K40 Ribbon Cable to [RPI-CNC](http://blog.protoneer.co.nz/raspberry-pi-cnc-board-hat/) adapter.  Since the RPI-CNC has strong pull-ups on the limit switch inputs (which are implemented using  Vishay TCST1030 photo interrupters, that have a very low current capacity), this board uses transistors to invert and amplify the signal.

This board also breaks out the X-axis stepper pins.

The files in the source are KiCAD 4.0

BOM: 

* 1 [TE connectivity 1-84534-2 connector](http://www.mouser.com/ProductDetail/TE-Connectivity/1-84534-2/?qs=%252btLcN0raKGUxLyp7hN%252bDsw%3d%3d&gclid=Cj0KEQjwv467BRCbkMvs5O3kioUBEiQAGDZHLy_EHALXi88dvzLZm20BSer-KzAdrLPKIezgcBq0TRYaAuvE8P8HAQ)
* 2 3K3 0805 SMD resistors
* 2 MMBT2222A SMD transistors
* 1 1x2 0.1" pitch header [OPTIONAL]
* 1 1x4 0.1" pitch header [OPTIONAL]
* 1 0805 SMD capacitor (noise reduction) [OPTIONAL]